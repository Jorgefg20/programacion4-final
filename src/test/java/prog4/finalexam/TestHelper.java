package prog4.finalexam;

import org.junit.Assert;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TestHelper {

    public WordTable insertAllInTable() {
        TestHelper testHelper = new TestHelper();
        List<Word> entries =  testHelper.retrieveAllEntries();
        WordTable table = new WordTable();

        int numberOfThreads = Runtime.getRuntime().availableProcessors();
        ExecutorService executorService = Executors.newFixedThreadPool(numberOfThreads);
        List<Callable<Void>> callables = new ArrayList<>();

        int blockSize = (entries.size() + numberOfThreads - 1)/numberOfThreads;

        for(int i=0;i<numberOfThreads;i++) {
            final int blockStartPosition = i;
            callables.add(()->{
                for(int j=0;j<blockSize;j++) {
                    int position = blockStartPosition * blockSize + j;
                    if (position<entries.size()) {
                        table.add(entries.get(position));
                    }
                }
                return null;
            });
        }

        try {
            executorService.invokeAll(callables);
        } catch (InterruptedException e) {
            e.printStackTrace();
            Assert.fail();
        }

        executorService.shutdown();

        return table;
    }

    List<Word> retrieveAllEntries() {
        URL resource = getClass().getClassLoader().getResource("words.csv");
        if (resource == null) {
            throw new IllegalArgumentException("file not found!");
        } else {
            File targetFile = new File(resource.getFile());
            try {
                List<String> lines = Files.readAllLines(Paths.get(resource.toURI()));
                return parseWordLines(lines);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    private List<Word> parseWordLines(List<String> lines) {
        List<Word> result = new ArrayList<>();
        for(int i=1;i<lines.size();i++) {
            String[] pieces = lines.get(i).split(",");
            Word entry = new Word();

            entry.setWord(pieces[0]);
            entry.setCount(Long.parseLong(pieces[1]));

            result.add(entry);
        }

        return result;
    }

    String retrievePlainTextFile(String fileName) {
        URL resource = getClass().getClassLoader().getResource(fileName);
        if (resource == null) {
            throw new IllegalArgumentException("file not found!");
        } else {
            File targetFile = new File(resource.getFile());
            try {
                return Files.readString(Paths.get(resource.toURI()));
            } catch (IOException e) {
                e.printStackTrace();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }

        return null;
    }
}
