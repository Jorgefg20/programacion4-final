package prog4.finalexam;

import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;

public class WordTable extends ArrayList<Word> {

    private static BTree<String, Word> bTree = new BTree<>();
   // int counter = 0;
    AtomicInteger atomicInteger = new AtomicInteger();

    @Override
    public boolean add(Word word) {
        synchronized (this) {
            word.setIncrementalId(atomicInteger.incrementAndGet());
            bTree.insert(word.getWord(), word);
            return super.add(word);
        }
    }

    // Problem 2, implement the B-Tree to search for values
    public Word findWord(String word) {
        /*
         * final String normalizedWord = word.toLowerCase(); for (Word row : this) { if
         * (normalizedWord.equals(row.getWord())) { return row; } }
         */

        return searchValue(word);
    }

    public Word searchValue(String word) {

        return bTree.get(word);
    }

}
