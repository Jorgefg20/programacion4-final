package prog4.finalexam;

public class Word {
    private int incrementalId;
    private String word;
    private long count;

    public int getIncrementalId() {
        return incrementalId;
    }

    public void setIncrementalId(int incrementalId) {
        this.incrementalId = incrementalId;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}
